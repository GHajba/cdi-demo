# README #

Mintaalkalmazások a [CDI című könyvemhez](https://leanpub.com/cdijavaee7).

A forráskódot vagy egy ZIP archívumként töltheti le, vagy lemásolhatja a Git repository-m is.

a) ZIP letöltés

b) Git

### Amire szükség lesz ###

 * Java SDK (8, esetleg 7)
 * Maven 3.x
 * JBoss WildFly 8.*