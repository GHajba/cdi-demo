package hu.japy.dev.cdi;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.jboss.weld.environment.se.bindings.Parameters;
import org.jboss.weld.environment.se.events.ContainerInitialized;
import org.slf4j.Logger;

/**
 * Greeter class managed by Weld CDI container.
 *
 */
// @Interceptors(value = { SimpleInterceptor.class, SecondInterceptor.class })
@Simple
public class Greeter {

    private final MyInterface myInterface;
    private MyInterface secondInterface;
    private final Logger logger;

    @Inject
    public Greeter(MyInterface myInterface, ProducedBean producedBean, final Logger logger) {
        System.out.println("constructor");
        this.myInterface = myInterface;
        producedBean.doSomething();
        this.logger = logger;
    }

    /**
     * Class prints a greeting message.
     *
     * @param event
     * @param parameters
     */
    // @Interceptors(value = { SimpleInterceptor.class, SecondInterceptor.class })
    @Simple
    // @SecondInc
    public void printHello(@Observes ContainerInitialized initialized, @Parameters final List<String> parameters) {

        printHello(parameters);
    }

    @PostConstruct
    void init() {
        System.out.print("init method: ");
        this.myInterface.printName();
        // this.secondInterface.printName();
    }

    @PreDestroy
    void cleanUp() {
        this.myInterface.printName();
    }

    public void printHello(final List<String> parameters) {

        String greet = "World";

        if (parameters != null && !parameters.isEmpty()) {
            greet = parameters.get(0);
        }

        System.out.println(String.format("Hello %s!", greet));
    }

    public void printHello() {
        printHello(null);
    }

    @Inject
    void setSecondInterface(@Second MyInterface myInf) {
        System.out.print("setter method: ");
        myInf.printName();
    }
}
