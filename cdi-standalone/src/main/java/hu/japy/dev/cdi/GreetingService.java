package hu.japy.dev.cdi;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author GHajba 2014 nov. 11
 *
 */
@Named
public class GreetingService {

    // @Inject
    // MyInterface inf;

    private final Greeter greeter;

    @PostConstruct
    public void init() {
        this.greeter.printHello();
    }

    @PreDestroy
    void cleanUp() {
        this.greeter.printHello();
    }

    @Inject
    GreetingService(Greeter greeter) {
        System.out.println("constructor");
        this.greeter = greeter;
    }

    public void greet() {
        System.out.println("printHello");
        this.greeter.printHello();
    }

    // @Inject
    // void setGreeter(Greeter greeter) {
    // System.out.println("setGreeter");
    // this.greeter = greeter;
    // }
}
