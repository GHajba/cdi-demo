package hu.japy.dev.cdi;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author GHajba
 *
 */
public class LoggerProducer {

    @Produces
    public static Logger getLogger(InjectionPoint injectionPoint) {
        final Class<?> tagetClass = injectionPoint.getMember().getDeclaringClass();
        System.out.println("Created logger for: " + tagetClass);
        return LoggerFactory.getLogger(tagetClass);
    }
}
