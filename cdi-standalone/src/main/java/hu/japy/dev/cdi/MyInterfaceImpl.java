package hu.japy.dev.cdi;

import javax.enterprise.inject.Default;

/**
 * @author GHajba 2014 nov. 12
 *
 */
@Default
// @Typed
// @Alternative
public class MyInterfaceImpl implements MyInterface {

    @Override
    public void printName() {
        System.out.println("MyInterfaceImpl");
    }

    // @Inject
    // void setGreeterService(GreetingService service) {
    // System.out.println("MyInterfaceImpl.setGreeterService");
    // }

}
