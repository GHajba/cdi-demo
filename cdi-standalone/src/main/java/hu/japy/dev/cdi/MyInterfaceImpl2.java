package hu.japy.dev.cdi;

/**
 * @author GHajba 2014 nov. 13
 *
 */
@Second
public class MyInterfaceImpl2 implements MyInterface {

    @Override
    public void printName() {
        System.out.println("MyInterfaceImpl2");
    }

}
