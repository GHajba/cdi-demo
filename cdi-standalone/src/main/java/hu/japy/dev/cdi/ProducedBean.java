package hu.japy.dev.cdi;

/**
 * @author GHajba 2014 dec. 18
 *
 */
public class ProducedBean {

    public void doSomething() {
        System.out.println(this);
    }
}
