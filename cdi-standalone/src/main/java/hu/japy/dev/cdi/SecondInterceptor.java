package hu.japy.dev.cdi;

import java.io.Serializable;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 * @author GHajba
 *
 */
@Simple
@Interceptor
public class SecondInterceptor implements Serializable {

    private static final long serialVersionUID = 701544868753009182L;

    @AroundInvoke
    public Object doSomething(InvocationContext context) throws Exception {
        System.out.println("beginning: " + this.getClass().getCanonicalName());
        final Object result = context.proceed();
        System.out.println("ending: " + this.getClass().getCanonicalName());

        return result;
    }

}
