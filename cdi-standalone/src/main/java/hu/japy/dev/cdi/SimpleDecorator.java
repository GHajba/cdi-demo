package hu.japy.dev.cdi;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.enterprise.inject.Default;
import javax.inject.Inject;

/**
 * @author GHajba
 *
 */
@Decorator
public class SimpleDecorator implements MyInterface {

    @Inject
    @Delegate
    // @Any
    @Default
    // @Second
    private MyInterface delegateInterface;

    @Override
    public void printName() {
        System.out.println(this.getClass().getCanonicalName());
    }
}
