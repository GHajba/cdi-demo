package hu.japy.dev.cdi;

import java.io.IOException;

import javax.ws.rs.Produces;

/**
 * @author GHajba 2014 dec. 18
 *
 */
public class SimpleProducer {

    @Produces
    public ProducedBean createProducedBean() throws IOException {
        throw new IOException();
        // return new ProducedBean();
    }

}
