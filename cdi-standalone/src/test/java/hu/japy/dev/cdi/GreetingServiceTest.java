package hu.japy.dev.cdi;

import org.junit.Test;

public class GreetingServiceTest {

    @Test
    public void greet() {
        final GreetingService greetingService = new GreetingService(new Greeter(new MyInterfaceImpl(), null, null));
        // greetingService.setGreeter(new Greeter());
        greetingService.greet();
    }
}
